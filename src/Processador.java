import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Processador {
	
	public static void main(String[] args) throws IOException {
	
		//criarArquivos();
		//listarArquivos();
		//gravarDados();
		//lerDados();
		//processamentoDeString();
		//gerarTabuada();
		//gravarNotas();
		File arquivo = new File("4106902_censo2010_rend.csv");
		Scanner leitor = new Scanner(arquivo);
		int total = 0;
		while (leitor.hasNext()){
			String linha = leitor.nextLine();
			if (linha.startsWith("Pessoas") && linha.contains("15 a 20 sal�rios m�nimos")){
				String[] vtr = linha.split(";");
				String qtde = vtr[1].replace(".", "");
				total = total + Integer.parseInt(qtde);
			}
		}
		leitor.close();
		//76281
		System.out.println("O total de pessoas que ganha de 15 a 20 sal�rios �: " + total);
	}

	private static void gravarNotas() throws IOException, FileNotFoundException {
		File arquivo = new File("notas.txt");
		FileWriter fw = new FileWriter(arquivo);
		BufferedWriter bw = new BufferedWriter(fw);
		
		Scanner leitor = new Scanner(System.in);
		
		for (int i = 1; i <= 3; i++) {
			System.out.println("Informe o nome do aluno " + i + ":");
			String nome = leitor.nextLine();
			bw.write(nome);
			System.out.println("Disciplina:");
			String disciplina = leitor.nextLine();
			bw.write(";" + disciplina);
			System.out.println("Nota:");
			String nota = leitor.nextLine();
			bw.write(";" + nota + "\n");
		}
		bw.close();
		fw.close();
		leitor.close();

		leitor = new Scanner(arquivo);
		while(leitor.hasNext()){
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			double nota = Double.parseDouble(dados[2]);
			if (nota >= 6){
				System.out.println(dados[0] + " est� aprovado!"); 
			}
		}
		
		leitor.close();
	}

	private static void gerarTabuada() throws IOException {
		File arquivo = new File("tabuada.txt");
		FileWriter fw = new FileWriter(arquivo);
		BufferedWriter bw = new BufferedWriter(fw);
		
		for (int i = 1; i <= 10; i++) {
			bw.write("Tabuada do " + i);
			bw.newLine();
			for (int j = 1; j <= 10; j++) {
				bw.write(i + " x " + j + ": " + (i * j));
				bw.newLine();
			}	
			bw.newLine();
		}
		bw.close();
		fw.close();
	}

	private static void processamentoDeString() {
		String txt = "Um texto apenas para teste;quebra aqui";
		
		//Mostra o texto;
		System.out.println(txt);
		
		//Mostra o tamanho do texto;
		System.out.println(txt.length());
		
		//Mostra o caractere no �ndice 10;
		System.out.println(txt.charAt(10));
		
		//Extrai parte do texto, substring;
		String substr = txt.substring(9, 15);
		System.out.println(substr);
		
		//Quebra o texto no ponto-e-v�rgula;
		String[] vetor = txt.split(";");
		System.out.println(vetor[0]);
		System.out.println(vetor[1]);
		
		//Concatena��o de String;
		String nova = vetor[1] +  "|" +  vetor[0];
		System.out.println(nova);
		
		String nova2 = vetor[1].concat("|").concat(vetor[0]);
		System.out.println(nova2);
		
		String[] vetor2 = nova.split(Pattern.quote("|"));
		System.out.println(vetor2[1]);
		
		for (int i = 0; i < txt.length(); i++) {
			char c = txt.charAt(i);
			System.out.println(c);
		}
		
		//Compara��o de igualdade;
		String a1 = new String("abc");
		String a2 = new String("abc");
		
		if (a1.equals(a2)){
			System.out.println("As strings s�o iguais");
		}
	}

	private static void lerDados() throws FileNotFoundException {
		File arquivo = new File("temp/temp.txt");
		//FileReader fr = new FileReader(arquivo);
		//BufferedReader br = new BufferedReader(fr);
		
		Scanner leitor = new Scanner(arquivo);
		
		while (leitor.hasNext()){
			String linha = leitor.nextLine();
			System.out.println(linha);
		}
		//br.close();
		//fr.close();
		leitor.close();
	}

	private static void gravarDados() throws IOException {
		File file = new File("temp/temp.txt");
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		
		for (int i = 1; i <= 1000; i++) {
			bw.write(i + "a linha de texto\n");	
		}
		
		bw.close();
		fw.close();
	}

	private static void listarArquivos() {
		File root = new File("c:/");
		File[] arquivos = root.listFiles();
		
		for (int i = 0; i < arquivos.length; i++) {
			File arquivo = arquivos[i];
			
			if (!arquivo.isDirectory()){
				System.out.println(arquivo);	
			}
			 
		}
	}

	private static void criarArquivos() throws IOException, FileNotFoundException {
		//Cria a pasta;
		File pasta = new File("c:/temp");
		pasta.mkdir();
		//pasta.delete();
		
		//Cria o arquivo dentro da pasta;
		File arquivoOrigem = new File("c:/temp/temp.txt");
		arquivoOrigem.createNewFile();
		//arquivo.delete();
		
		File pastaCopia = new File("c:/copia");
		pastaCopia.mkdir();
		
		File arquivoDestino = new File("c:/copia/temp.txt");
		FileOutputStream fos = new FileOutputStream(arquivoDestino);
		Files.copy(arquivoOrigem.toPath(), fos);
	}
}